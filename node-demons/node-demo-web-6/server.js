﻿'use strict';

const request = require('request');
const zlib = require('zlib');
const fs = require('fs');

const req = request('http://www.google.com');

let dataEventCount = 0;
const usePipe = process.argv[2];
const compress = process.argv[3];
const filename = process.argv[4];

if (true.toString() === usePipe.toLowerCase()) {
    let zipStream = null;

    if (true.toString() === compress.toLowerCase()) {
        zipStream = zlib.createGzip();
    }

    if (zipStream) {
        req.pipe(zipStream).pipe(fs.createWriteStream(filename));
        return;
    }

    req.pipe(process.stdout);
    return;
}

req.on('data',
    (chunk) => {
        dataEventCount++;
    console.log(`>>>Data ${chunk}`);
});

req.on('end',
    () => {
    console.log(`>>>Done (${dataEventCount} data events) >>>`);
    });