﻿'use strict';

exports.logRequest = () => {
    return (req, res, next) => {
        console.log(`Incoming request on ${req.url}`);
        next();
    }
}

exports.logResponse = () => {
    return (req, res, next) => {
        console.log(`Request on ${req.url} returned ${res.statusCode}`);
        next();
    }
}