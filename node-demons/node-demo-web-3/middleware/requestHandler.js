﻿'use strict';

const url = require('url');

const routeMap = new Map();
routeMap.set('/', index);
routeMap.set('/about', about);
routeMap.set('notFound', notFound);


function index(req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('This is the index page.');
}

function about(req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('This is the about page.');
}

function notFound(req, res) {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('The requested URL was not found.');
}

exports.handleRequest = () => {
    return (req, res, next) => {
        var path = url.parse(req.url).pathname;
        var func = routeMap.get(path);
        
        if (func === undefined) {
            func = routeMap.get('notFound');
        }
        
        func(req, res);
        
        next();
    }
}