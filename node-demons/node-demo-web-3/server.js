﻿'use strict';

const http = require('http');
const requestHandler = require('./middleware/requestHandler');
const logger = require('./middleware/logger');
const connect = require('connect');

const middleware = connect()
    .use(logger.logRequest())
    .use(requestHandler.handleRequest())
    .use(logger.logResponse());

const port = 1337;
const host = '127.0.0.1';

http.createServer(middleware)
    .listen(port,
        host,
        (err) => {
            if (err) {
                console.log(err);
                return;
            }

            console.log(`Server listening on ${host}:${port}`);
        });