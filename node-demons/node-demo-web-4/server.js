﻿'use strict';

const express = require('express');
const app = express();
const config = require('./config');
const helpers = require('./helpers');
const scoreData = require('./data/scores.json');
const host = '127.0.0.1';

app.set('views', './views');
app.set('view engine', 'jade');

app.get(
    '/api/v1/scores/:team',
    (req, res) => {
        const team = req.params.team.toUpperCase();
        const scores = helpers.scoresForTeam(team, scoreData);

        if (!helpers.any(scores)) {
            res.send(404);
            return;
        }

        res.send(scores);
    });

app.listen(
    config.node.port,
    host,
    () => {
        console.log(`Listening on ${host}:${config.node.port}`);
    });