﻿'use strict';

const config = {
    node: {}
};

config.node.port = process.env.port || 1337;
module.exports = config;