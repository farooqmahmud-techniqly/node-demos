﻿'use strict';

exports.any = (collection) => {
    if (!collection || collection.length === 0) {
        return false;
    }

    return true;
}

exports.scoresForTeam = (team, scores) => {
    return scores.filter(d => d.homeTeam.toUpperCase() === team || d.awayTeam.toUpperCase() === team);
}