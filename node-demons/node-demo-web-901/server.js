﻿'use strict';

const http = require('http');
const io = require('socket.io');
const ioClient = require('socket.io-client');
const Random = require('random-js');

const port = process.env.port || 3399;
const host = process.env.IP ||  '127.0.0.1';

const app = http.createServer((req, res) => {
        var client = ioClient.connect(`http://${host}:${port}`, { reconnect: true });
        var a = [];

        client.on('quote',
            (q) => {
                a.push(q);
                console.log(`Received quote event from server -- ${q.ts}:${q.p} -- ${a.length} quotes in stored.`);
            });

        if (req.url === '/api/person/1') {
            res.writeHead('200', { 'Content-Type': 'text/plain' });
            res.end('{"id":1}');
        } else if (req.url === '/api/person/1/address') {
            res.writeHead('200', { 'Content-Type': 'text/plain' });
            res.end('{"id":1, "address":"123 Main St."}');
        } else if (req.url === '/api/quote') {
            res.writeHead('200', { 'Content-Type': 'text/plain' });
            res.end('quote');
        } else {
            res.writeHead('404', { 'Content-Type': 'text/plain' });
            res.end('{ "error": "The requested resource was not found." }');
        }

    })
    .listen(port, host);

const ioApp = io.listen(app);
const delay = 5000;

ioApp.sockets.on(('connection'),
(socket) => {
    setInterval(() => {
        const now = Date.now();
        const r = new Random();
        const price = r.integer(100, 150);
        console.log(`${now}:${price}`);
        socket.emit('quote', {ts:now, p:price});
    }, delay);
});