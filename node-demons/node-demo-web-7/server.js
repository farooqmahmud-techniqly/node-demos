﻿'use strict';

process.stdout.write(`[${process.pid}]>>>   `);
process.stdin.resume();
process.stdin.setEncoding('utf-8');

process.stdin.on('data',
(chunk) => {
    process.stdout.write(`Data >>> ${chunk}`);
});

process.stdin.on('end',
() => {
    process.stderr.write('Done\n');
});