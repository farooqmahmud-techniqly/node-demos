﻿'use strict';

const should = require('should');

const user = {
    name: 'Farooq',
    pets: ['tobi', 'nikki', 'dot', 'elaine']
};


describe('JavaScript tests',
    function() {
        it('should add two numbers correctly',
            function() {
                (3 + 4).should.equal(7);
            });

        it('should multiply two numbers correctly',
            function (done) {
                (3 * 4).should.equal(12);
                done();
            });
    });