﻿'use strict';

const express = require('express');
const favicon = require('express-favicon');

const app = express();
const port = process.env.port || 1337;
const host = "127.0.0.1";

app.use('/static', express.static('static'));
app.use(favicon(__dirname + '/static/favicon.ico'));

app.get(
    '/',
    (request, response) => {
        console.log('Incoming request for %s', request.url);
        response.end('Hello I\'m Express!');
    });

app.listen(
    port,
    host,
    () => {
        console.log('Server is listening on %s:%s', host, port);
    });
