﻿'use strict';

const http = require('http');
const serveStatic = require('./serverStatic.js');
const e = require('./error.js');

http.createServer(function (request, response) {
    console.log('Incoming request on ' + request.url);

    if (serveStatic.canServe(request)) {
        serveStatic.serveStatic(request, response);
        return;
    } else {
        e.badRequest(response, 'File not found.');
    }
    
})