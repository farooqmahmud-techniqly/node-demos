﻿'use strict';

const fs = require('fs');
const e = require('./error.js');

exports.canServe = function (request) {
    return request.url.startsWith('/static') ||
        request.url === '/favicon.ico';
};

exports.serveStatic = function (request, response) {
        var url = request.url.substring(1);

        if (url === "favicon.ico") {
            url = 'static/favicon.ico';
        }

        fs.readFile(url, (error, data) => {
        if (error) {
            e.badRequest(response, error);
            return;
        }
        response.setHeader('Content-Type', 'text/html');
        response.end(data);
    });    
}