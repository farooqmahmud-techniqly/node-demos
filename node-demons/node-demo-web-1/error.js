﻿'use strict';

const setStatus = function (response, error, statusCode) {
    console.log(error);
    response.statusCode = statusCode;
    response.end();
}

exports.badRequest = function (response, error) {
    setStatus(response, error, 404);
}

