﻿'use strict';

const http = require('http');
const port = process.env.port || 3399;
const host = process.env.IP || '127.0.0.1';

http.createServer((req, res) => {
        if (req.url === '/api/person/1') {
            res.writeHead('200', { 'Content-Type': 'text/plain' });
            res.end('{"id":1}');
        } else if (req.url === '/api/person/1/address') {
            res.writeHead('200', { 'Content-Type': 'text/plain' });
            res.end('{"id":1, "address":"123 Main St."}');
        } else {
            res.writeHead('404', { 'Content-Type': 'text/plain' });
            res.end('{ "error": "The requested resource was not found." }');
        }

    })
    .listen(port, host);