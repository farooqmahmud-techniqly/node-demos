﻿'use strict';

const http = require('http');

const options = {
    'host': 'www.google.com',
    'port': 80,
    'path': '/',
    'method': 'GET'
};

http.get(options,
(res) => {
    console.log(res.statusCode);
    res.pipe(process.stdout);
});