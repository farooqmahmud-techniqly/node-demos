﻿'use strict';

exports.logRequest = function() {
    return (request, response, next) => {
        console.log(`Incoming request for ${request.url}`);
        next();
    }
}

exports.logResponse = function () {
    return (request, response, next) => {
        console.log(`Response status code: ${response.statusCode}`);
        next();
    }
}