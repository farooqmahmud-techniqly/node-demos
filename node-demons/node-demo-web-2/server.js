﻿'use strict';

const express = require('express');
const logger = require('./logger.js');
var tableData = require('./data/tableData.json');
const bodyParser = require('body-parser');
const uuid = require('uuid');
const _ = require('lodash');
const app = express();

app.set('views', './views');
app.set('view engine', 'jade');

const port = process.env.port || 1337;
const host = "127.0.0.1";

app.use(logger.logRequest());
app.use(logger.logResponse());
app.use(express.static('node_modules/bootstrap/dist'));
app.use(bodyParser.urlencoded({extended: true}));

app.get(
    '/',
    (request, response) => {
        response.render('index', {title: "Welcome!"});
    }
);

app.get(
    '/index',
    (request, response) => {
        response.render('index', {title :"Welcome!"});
    }
);

app.get(
    '/room',
    (request, response) => {
        response.render('room', {title: "Chat Room", tableData: tableData});
    }
);

app.get(
    '/add',
    (request, response) => {
        response.render('add', { title: "Add New Room" });
    });

app.post(
    '/add',
    (request, response) => {
        const  newData = {
            name: request.body.name,
            email: request.body.email,
            id: uuid.v4()
        };

        tableData.push(newData);
        response.redirect('/room');
    });

app.get(
    '/delete/:id',
    (request, response) => {
        tableData = tableData.filter(d => d.id !== request.params.id);
        response.redirect('/room');
    });

app.get(
    '/edit/:id',
    (request, response) => {
        var data = _.find(tableData, d => d.id === request.params.id);

        if (!data) {
            response.sendStatus(404);
            return;
        }
        response.render('edit', { data });
    });

app.post(
    '/edit/:id',
    (request, response) => {
        var data = _.find(tableData, d => d.id === request.params.id);

        if (!data) {
            response.sendStatus(404);
            return;
        }

        data.email = request.body.email;
        response.redirect('/room');
    });

app.listen(
    port,
    host,
    () => {
        console.log(`Server is listening on ${host}:${port}`);
    });