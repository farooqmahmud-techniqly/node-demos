﻿'use strict';
const util = require('util');
const EventEmitter = require('events').EventEmitter;

function Resource(maxEvents, delay) {
    const self = this;

    process.nextTick(() => {
        let currentCount = 0;
        self.emit('start');
        const t = setInterval(() => {
                self.emit('data', ++currentCount);

                if (currentCount === maxEvents) {
                    self.emit('end', currentCount);
                    clearInterval(t);
                }
            },
            delay);
    });
};

util.inherits(Resource, EventEmitter);
module.exports = Resource;