﻿'use strict';

const Resource = require('./resource');

const eventsToPublish = 20;
const delay = 10;
const r = new Resource(eventsToPublish, delay);

r.on('start',
() => {
    console.log('Started');
    });

r.on('data',
(data) => {
    console.log(`Received data ${data}`);
    });

r.on('end',
(count) => {
    console.log(`Done processing ${count} events.`);
});